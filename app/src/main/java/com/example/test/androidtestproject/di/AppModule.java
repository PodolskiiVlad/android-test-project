package com.example.test.androidtestproject.di;

import android.content.Context;

import com.example.test.androidtestproject.core.NewsApplication;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule {

    @Provides
    Context provideContext(NewsApplication app){
        return app;
    }
}
