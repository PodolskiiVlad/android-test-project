package com.example.test.androidtestproject.di.module.view;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.view.ui.ArticleActivity;
import com.example.test.androidtestproject.viewmodel.ArticleActivityViewModel;
import com.example.test.androidtestproject.viewmodel.factory.ArticleActivityViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class ArticleFragmentModule {

    @Provides
    int provideItemId(ArticleActivity articleActivity) {
        return articleActivity.getIntent().getIntExtra(ArticleActivity.KEY_ITEM_ID, 0);
    }

    @Provides
    ViewModelStoreOwner provideStoreOwner(ArticleActivity dialog) {
        return dialog;
    }

    @Provides
    ArticleActivityViewModelFactory provideFactory(ArticlePreviewRepository repository, int itemId) {
        return new ArticleActivityViewModelFactory(repository, itemId);
    }

    @Provides
    ViewModelProvider provideViewModelProvider(ViewModelStoreOwner viewModelStoreOwner, ArticleActivityViewModelFactory factory) {
        return new ViewModelProvider(viewModelStoreOwner, factory);
    }

    @Provides
    ArticleActivityViewModel provideViewModel(ViewModelProvider viewModelProvider) {
        return viewModelProvider.get(ArticleActivityViewModel.class);
    }
}
