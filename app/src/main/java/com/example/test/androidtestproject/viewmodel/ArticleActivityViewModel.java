package com.example.test.androidtestproject.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;

import io.reactivex.disposables.CompositeDisposable;

public class ArticleActivityViewModel extends ViewModel {

    private final MutableLiveData<String> title;
    private final MutableLiveData<String> description;
    private final MutableLiveData<String> imageUrl;
    private final MutableLiveData<String> sourceUrl;
    private final MutableLiveData<String> author;
    private final MutableLiveData<String> publishedAt;

    private final CompositeDisposable disposable;

    private final ArticlePreviewRepository repository;
    private final int itemId;

    public ArticleActivityViewModel(ArticlePreviewRepository repository, int itemId) {
        this.repository = repository;
        this.itemId = itemId;

        title = new MutableLiveData<>();
        description = new MutableLiveData<>();
        imageUrl = new MutableLiveData<>();
        sourceUrl = new MutableLiveData<>();
        author = new MutableLiveData<>();
        publishedAt = new MutableLiveData<>();

        disposable = new CompositeDisposable();
    }

    public void getArticle() {
        disposable.add(repository.getArticle(itemId).subscribe(uiArticle -> {
            title.postValue(uiArticle.getTitle());
            description.postValue(uiArticle.getDescription());
            imageUrl.postValue(uiArticle.getImageUrl());
            author.postValue(uiArticle.getAuthor());
            publishedAt.postValue(getDate(uiArticle.getPublishedAt()));
            sourceUrl.postValue(uiArticle.getSourceUrl());
        }, throwable -> Log.e(getClass().getSimpleName(), "ArticleActivityViewModel: ", throwable)));
    }

    public LiveData<String> getTitle() {
        return title;
    }

    public LiveData<String> getDescription() {
        return description;
    }

    public LiveData<String> getImageUrl() {
        return imageUrl;
    }

    public LiveData<String> getAuthor() {
        return author;
    }

    public LiveData<String> getPublishedAt() {
        return publishedAt;
    }

    public MutableLiveData<String> getSourceUrl() {
        return sourceUrl;
    }

    public void jumpToSource(Context context) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sourceUrl.getValue()));
        context.startActivity(browserIntent);
    }

    //I didn't want to waste much time on implementation
    private String getDate(String dateStr) {
        String[] splitted = dateStr.split("T");
        String result = splitted[0];
        return result.replace("-", ".");
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }
}
