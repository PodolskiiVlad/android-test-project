package com.example.test.androidtestproject.view.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.test.androidtestproject.R;
import com.example.test.androidtestproject.databinding.ActivityArticleBinding;
import com.example.test.androidtestproject.viewmodel.ArticleActivityViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class ArticleActivity extends DaggerAppCompatActivity {

    public static final String KEY_ITEM_ID = "ITEM_ID";

    @Inject
    ArticleActivityViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityArticleBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_article);

        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.getArticle();
    }
}
