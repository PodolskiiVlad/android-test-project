package com.example.test.androidtestproject.data.network.retrofit;

import com.example.test.androidtestproject.data.model.network.response.RemoteResponseHeadlineArticles;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.QueryParameters;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsLoaderApi{

    @GET("/v2/top-headlines")
    Flowable<RemoteResponseHeadlineArticles> getBreakingNewsArticles(
            @Category @Query(QueryParameters.QUERY_PARAMETER_CATEGORY) String category,
            @Language @Query(QueryParameters.QUERY_PARAMETER_LANGUAGE) String language,
            @Country @Query(QueryParameters.QUERY_PARAMETER_COUNTRY) String country
    );
}
