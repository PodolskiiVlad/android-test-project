package com.example.test.androidtestproject.data.model.ui;

import androidx.annotation.NonNull;

import java.util.Objects;

public class UIArticlePreview {

    private int id;

    @NonNull
    private String title;

    @NonNull
    private String imageUrl;

    public UIArticlePreview(@NonNull String title, @NonNull String imageUrl, int id) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UIArticlePreview)) return false;
        UIArticlePreview that = (UIArticlePreview) o;
        return title.equals(that.title) &&
                imageUrl.equals(that.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, imageUrl);
    }
}
