package com.example.test.androidtestproject.core;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.example.test.androidtestproject.view.ui.ArticleActivity;
import com.example.test.androidtestproject.view.ui.NewsFilterDialog;

public class Launcher {

    public static DialogFragment startNewsFilterDialog(FragmentActivity activity) {
        DialogFragment newsFilterDialog = NewsFilterDialog.getInstance();
        newsFilterDialog.show(activity.getSupportFragmentManager(), null);
        return newsFilterDialog;
    }

    public static void startArticleActivity(Context context, int articleId) {
        Intent intent = new Intent(context, ArticleActivity.class);
        intent.putExtra(ArticleActivity.KEY_ITEM_ID, articleId);

        context.startActivity(intent);
    }
}
