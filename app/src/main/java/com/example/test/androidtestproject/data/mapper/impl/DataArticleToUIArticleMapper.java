package com.example.test.androidtestproject.data.mapper.impl;

import com.example.test.androidtestproject.data.mapper.Mapper;
import com.example.test.androidtestproject.data.model.data.DataArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticle;

import javax.inject.Inject;

public class DataArticleToUIArticleMapper implements Mapper<DataArticle, UIArticle> {

    @Inject
    DataArticleToUIArticleMapper() {
    }

    @Override
    public UIArticle map(DataArticle inputType) {
        String title = inputType.getTitle();
        String publishedAt = inputType.getPublishedAt();
        String sourceUrl = inputType.getSourceUrl();

        String author = "";
        String description = "";
        String imageUrl = "";

        if (inputType.getAuthor() != null) {
            author = inputType.getAuthor();
        }

        if (inputType.getDescription() != null) {
            description = inputType.getDescription();
        }

        if (inputType.getUrlToImage() != null) {
            imageUrl = inputType.getUrlToImage();
        }

        return new UIArticle(imageUrl, title, description, publishedAt, author, sourceUrl);
    }
}
