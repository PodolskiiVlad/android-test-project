package com.example.test.androidtestproject.di.module.model;

import com.example.test.androidtestproject.data.mapper.ListMapper;
import com.example.test.androidtestproject.data.mapper.Mapper;
import com.example.test.androidtestproject.data.mapper.impl.DataArticleToUIArticleMapper;
import com.example.test.androidtestproject.data.mapper.impl.DataArticleToUIPreviewArticleMapper;
import com.example.test.androidtestproject.data.mapper.impl.ListMapperImpl;
import com.example.test.androidtestproject.data.mapper.impl.RemoteArticleToDataArticleMapper;
import com.example.test.androidtestproject.data.model.data.DataArticle;
import com.example.test.androidtestproject.data.model.network.response.RemoteArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;

import dagger.Binds;
import dagger.Module;

@Module
abstract class MapperModule {

    @Binds
    abstract Mapper<RemoteArticle, DataArticle> bindRemoteToDataMapper(RemoteArticleToDataArticleMapper mapper);

    @Binds
    abstract Mapper<DataArticle, UIArticlePreview> bindDataTiPreviewMapper(DataArticleToUIPreviewArticleMapper mapper);

    @Binds
    abstract Mapper<DataArticle, UIArticle> bindDataToArticleMapper(DataArticleToUIArticleMapper mapper);

    @Binds
    abstract ListMapper<RemoteArticle, DataArticle> bindRemoteToDataListMapper(ListMapperImpl<RemoteArticle, DataArticle> listMapper);

    @Binds
    abstract ListMapper<DataArticle, UIArticlePreview> bindDataToPreviewListMapper(ListMapperImpl<DataArticle, UIArticlePreview> listMapper);
}
