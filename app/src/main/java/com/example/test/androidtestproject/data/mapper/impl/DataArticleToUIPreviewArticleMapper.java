package com.example.test.androidtestproject.data.mapper.impl;

import com.example.test.androidtestproject.data.mapper.Mapper;
import com.example.test.androidtestproject.data.model.data.DataArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;

import javax.inject.Inject;

public class DataArticleToUIPreviewArticleMapper implements Mapper<DataArticle, UIArticlePreview> {

    @Inject
    DataArticleToUIPreviewArticleMapper() {
    }

    @Override
    public UIArticlePreview map(DataArticle inputType) {
        String title = inputType.getTitle();
        String imageUrl = inputType.getUrlToImage();
        int id = inputType.getId();

        if (title == null) {
            title = DEFAULT_STRING;
        }

        if (imageUrl == null) {
            imageUrl = DEFAULT_STRING;
        }


        return new UIArticlePreview(title, imageUrl, id);
    }
}
