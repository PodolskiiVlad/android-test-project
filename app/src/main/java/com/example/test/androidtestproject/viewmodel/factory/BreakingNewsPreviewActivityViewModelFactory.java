package com.example.test.androidtestproject.viewmodel.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.viewmodel.BreakingNewsPreviewActivityViewModel;

public class BreakingNewsPreviewActivityViewModelFactory implements ViewModelProvider.Factory {

    private final ArticlePreviewRepository repository;

    public BreakingNewsPreviewActivityViewModelFactory(ArticlePreviewRepository repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new BreakingNewsPreviewActivityViewModel(repository);
    }
}
