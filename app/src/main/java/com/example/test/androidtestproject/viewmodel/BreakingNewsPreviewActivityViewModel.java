package com.example.test.androidtestproject.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test.androidtestproject.core.Launcher;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;
import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BreakingNewsPreviewActivityViewModel extends ViewModel {

    private final MutableLiveData<List<UIArticlePreview>> articleList;
    private final MutableLiveData<String> errorData;
    private final MutableLiveData<Boolean> dataIsLoading;

    private final ArticlePreviewRepository repository;

    private final CompositeDisposable compositeDisposable;

    @Inject
    public BreakingNewsPreviewActivityViewModel(ArticlePreviewRepository repository) {
        this.repository = repository;

        articleList = new MutableLiveData<>(new ArrayList<>());
        dataIsLoading = new MutableLiveData<>(false);
        errorData = new MutableLiveData<>();

        compositeDisposable = new CompositeDisposable();

        compositeDisposable.add(
                repository.getArticleFlowable()
                        .subscribe(
                                articlePreviews -> {
                                    articleList.postValue(articlePreviews);
                                    dataIsLoading.postValue(false);
                                },
                                throwable -> {
                                    Log.e(getClass().getSimpleName(), "BreakingNewsPreviewActivityViewModel: ", throwable);
                                    dataIsLoading.postValue(false);
                                }));

        compositeDisposable.add(
                repository.getErrorObservable()
                        .subscribe(
                                remoteError -> errorData.postValue(remoteError.getMessage()),
                                throwable -> Log.e(getClass().getSimpleName(),
                                        "BreakingNewsPreviewActivityViewModel: ", throwable)));
    }

    public void updateArticleList() {
        dataIsLoading.postValue(true);
        repository.updateArticleList();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
    }

    public LiveData<List<UIArticlePreview>> getArticleListLiveData() {
        return articleList;
    }

    public LiveData<String> getErrorLiveData() {
        return errorData;
    }

    public LiveData<Boolean> getDataIsLoadingLiveData() {
        return dataIsLoading;
    }

    public void onArticleClick(Context context, int itemId) {
        Launcher.startArticleActivity(context, itemId);
    }

    public void onFilterClick(FragmentActivity fragmentActivity) {
        Launcher.startNewsFilterDialog(fragmentActivity).getLifecycle().addObserver((LifecycleEventObserver) (source, event) -> {
            if (event.equals(Lifecycle.Event.ON_DESTROY)) {
                updateArticleList();
            }
        });
    }
}
