package com.example.test.androidtestproject.data.network.retrofit;

public interface NetworkProperties {

    String API_KEY = "d5aee91e5a984227a641457c7dd1ce50";
    String BASE_URL = "https://newsapi.org";
}
