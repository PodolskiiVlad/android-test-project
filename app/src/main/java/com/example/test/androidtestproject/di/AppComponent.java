package com.example.test.androidtestproject.di;

import com.example.test.androidtestproject.core.NewsApplication;
import com.example.test.androidtestproject.di.module.model.RepositoryModule;
import com.example.test.androidtestproject.di.scope.AppScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@AppScope
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivityBuildersModule.class,
        DialogBuildersModule.class,
        RepositoryModule.class,
        AppModule.class})
public interface AppComponent extends AndroidInjector<NewsApplication> {

    @Override
    void inject(NewsApplication instance);

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application(NewsApplication application);

        AppComponent build();
    }
}
