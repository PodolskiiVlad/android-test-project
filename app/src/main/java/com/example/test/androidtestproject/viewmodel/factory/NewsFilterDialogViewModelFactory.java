package com.example.test.androidtestproject.viewmodel.factory;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.viewmodel.NewsFilterDialogViewModel;

public class NewsFilterDialogViewModelFactory implements ViewModelProvider.Factory {

    private final ArticlePreviewRepository repository;
    private final Context context;

    public NewsFilterDialogViewModelFactory(ArticlePreviewRepository repository, Context context) {
        this.repository = repository;
        this.context = context;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new NewsFilterDialogViewModel(repository, context);
    }
}
