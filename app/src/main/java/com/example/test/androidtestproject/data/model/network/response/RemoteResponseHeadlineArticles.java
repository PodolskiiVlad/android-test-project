package com.example.test.androidtestproject.data.model.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RemoteResponseHeadlineArticles {

    @SerializedName("status")
    private String status;

    @SerializedName("totalResults")
    private int totalResults;

    @SerializedName("articles")
    private List<RemoteArticle> articleList;

    public String getStatus() {
        return status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<RemoteArticle> getArticleList() {
        return articleList;
    }
}
