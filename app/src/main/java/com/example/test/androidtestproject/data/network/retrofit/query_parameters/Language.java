package com.example.test.androidtestproject.data.network.retrofit.query_parameters;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;

import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.AR;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.DE;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.EN;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.ES;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.FR;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.HE;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.IT;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.NL;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.NO;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.PT;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.RU;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.SE;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language.ZH;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@StringDef({
        AR, DE, EN, ES, FR, HE, IT, NL, NO, PT, RU, SE, ZH
})

public @interface Language {
    String AR = "ar";
    String DE = "de";
    String EN = "en";
    String ES = "es";
    String FR = "fr";
    String HE = "he";
    String IT = "it";
    String NL = "nl";
    String NO = "no";
    String PT = "pt";
    String RU = "ru";
    String SE = "se";
    String ZH = "zh";
}
