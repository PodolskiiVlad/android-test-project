package com.example.test.androidtestproject.data.network.retrofit;

import com.example.test.androidtestproject.data.model.network.RemoteError;

public interface RemoteErrorParser {

    RemoteError parseError(Throwable e);
}
