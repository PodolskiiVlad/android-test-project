package com.example.test.androidtestproject.di.module.view;

import android.content.Context;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.view.ui.NewsFilterDialog;
import com.example.test.androidtestproject.viewmodel.NewsFilterDialogViewModel;
import com.example.test.androidtestproject.viewmodel.factory.NewsFilterDialogViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsFilterDialogModule {

    @Provides
    ViewModelStoreOwner provideStoreOwner(NewsFilterDialog dialog) {
        return dialog;
    }

    @Provides
    NewsFilterDialogViewModelFactory provideFactory(ArticlePreviewRepository repository, Context context) {
        return new NewsFilterDialogViewModelFactory(repository, context);
    }

    @Provides
    ViewModelProvider provideViewModelProvider(ViewModelStoreOwner viewModelStoreOwner, NewsFilterDialogViewModelFactory factory) {
        return new ViewModelProvider(viewModelStoreOwner, factory);
    }

    @Provides
    NewsFilterDialogViewModel provideViewModel(ViewModelProvider viewModelProvider) {
        return viewModelProvider.get(NewsFilterDialogViewModel.class);
    }
}
