package com.example.test.androidtestproject.viewmodel.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.viewmodel.ArticleActivityViewModel;

public class ArticleActivityViewModelFactory implements ViewModelProvider.Factory {

    private final ArticlePreviewRepository repository;
    private final int itemId;

    public ArticleActivityViewModelFactory(ArticlePreviewRepository repository, int itemId) {
        this.repository = repository;
        this.itemId = itemId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ArticleActivityViewModel(repository, itemId);
    }
}
