package com.example.test.androidtestproject.data.network.retrofit.query_parameters;

public class QueryParameters {

    public static final String QUERY_PARAMETER_API_KEY = "apiKey";

    public static final String QUERY_PARAMETER_CATEGORY = "category";
    public static final String QUERY_PARAMETER_LANGUAGE = "language";
    public static final String QUERY_PARAMETER_COUNTRY = "country";
}
