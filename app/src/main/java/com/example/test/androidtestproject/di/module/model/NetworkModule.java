package com.example.test.androidtestproject.di.module.model;

import com.example.test.androidtestproject.data.network.retrofit.NetworkProperties;
import com.example.test.androidtestproject.data.network.retrofit.NewsLoaderApi;
import com.example.test.androidtestproject.data.network.retrofit.RemoteErrorParser;
import com.example.test.androidtestproject.data.network.retrofit.RemoteErrorParserImpl;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.QueryParameters;
import com.example.test.androidtestproject.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    OkHttpClient provideInterceptor() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(chain -> {
            Request oldRequest = chain.request();
            HttpUrl oldUrl = oldRequest.url();

            HttpUrl newUrl = oldUrl.newBuilder()
                    .addQueryParameter(QueryParameters.QUERY_PARAMETER_API_KEY, NetworkProperties.API_KEY)
                    .build();

            Request.Builder requestBuilder = oldRequest.newBuilder();
            Request newRequest = requestBuilder.url(newUrl).build();

            return chain.proceed(newRequest);
        });

        return client.build();
    }

    @Provides
    @AppScope
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(NetworkProperties.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @AppScope
    NewsLoaderApi provideNewsLoaderApi(Retrofit retrofit) {
        return retrofit.create(NewsLoaderApi.class);
    }

    @Provides
    RemoteErrorParser provideErrorParser(Retrofit retrofit) {
        return new RemoteErrorParserImpl(retrofit);
    }

}
