package com.example.test.androidtestproject.di.module.model;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.data.repository.ArticlePreviewRepositoryImpl;
import com.example.test.androidtestproject.di.scope.AppScope;

import dagger.Binds;
import dagger.Module;

@Module(includes = {LocalStorageModule.class, NetworkModule.class, MapperModule.class})
public abstract class RepositoryModule {

    @Binds
    @AppScope
    abstract ArticlePreviewRepository bindArticlePreviewRepository(ArticlePreviewRepositoryImpl repository);
}
