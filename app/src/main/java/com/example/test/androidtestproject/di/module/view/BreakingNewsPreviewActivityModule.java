package com.example.test.androidtestproject.di.module.view;

import android.content.Context;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;
import com.example.test.androidtestproject.view.adapter.PreviewArticleAdapter;
import com.example.test.androidtestproject.view.ui.BreakingNewsPreviewActivity;
import com.example.test.androidtestproject.viewmodel.BreakingNewsPreviewActivityViewModel;
import com.example.test.androidtestproject.viewmodel.factory.BreakingNewsPreviewActivityViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class BreakingNewsPreviewActivityModule {

    @Provides
    ViewModelStoreOwner provideViewModelStoreOwner(BreakingNewsPreviewActivity activity) {
        return activity;
    }

    @Provides
    PreviewArticleAdapter provideAdapter() {
        return new PreviewArticleAdapter();
    }

    @Provides
    RecyclerView.LayoutManager provideLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    BreakingNewsPreviewActivityViewModelFactory provideFactory(ArticlePreviewRepository repository) {
        return new BreakingNewsPreviewActivityViewModelFactory(repository);
    }

    @Provides
    ViewModelProvider provideViewModelProvider(ViewModelStoreOwner storeOwner, BreakingNewsPreviewActivityViewModelFactory factory) {
        return new ViewModelProvider(storeOwner, factory);
    }

    @Provides
    BreakingNewsPreviewActivityViewModel provideViewModel(ViewModelProvider viewModelProvider) {
        return viewModelProvider.get(BreakingNewsPreviewActivityViewModel.class);
    }
}
