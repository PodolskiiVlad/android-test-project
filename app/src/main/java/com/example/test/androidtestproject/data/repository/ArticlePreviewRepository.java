package com.example.test.androidtestproject.data.repository;

import com.example.test.androidtestproject.data.model.network.RemoteError;
import com.example.test.androidtestproject.data.model.ui.UIArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface ArticlePreviewRepository {

    Flowable<List<UIArticlePreview>> getArticleFlowable();

    Single<UIArticle> getArticle(int articleId);

    Observable<RemoteError> getErrorObservable();

    void updateArticleList();

    void savePreferences(@Category String category, @Language String language, @Country String country);

    @Country String getCountryPreference();
    @Language String getLanguagePreference();
    @Category String getCategoryPreference();
}
