package com.example.test.androidtestproject.data.model.network;

import com.google.gson.annotations.SerializedName;

public class RemoteError {

    @SerializedName("code")
    private String code;

    @SerializedName(value = "message", alternate = "Something went wrong")
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
