package com.example.test.androidtestproject.data.network.retrofit;

import android.util.Log;

import com.example.test.androidtestproject.data.model.network.RemoteError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RemoteErrorParserImpl implements RemoteErrorParser{

    private final Retrofit retrofit;

    @Inject
    public RemoteErrorParserImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public RemoteError parseError(Throwable e) {
        RemoteError remoteError;

        if (e instanceof HttpException) {
            Response response = ((HttpException)e).response();
            remoteError = createFromResponse(response);
        } else {
            Log.e(getClass().getSimpleName(), "parseError: ", e);
            remoteError = new RemoteError();
        }

        return remoteError;
    }

    private RemoteError createFromResponse(Response<?> response) {
        Converter<ResponseBody, RemoteError> converter =
                retrofit.responseBodyConverter(RemoteError.class, new Annotation[0]);

        RemoteError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "createFromResponse: ", e);
            return new RemoteError();
        }

        return error;
    }
}
