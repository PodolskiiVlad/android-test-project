package com.example.test.androidtestproject.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.androidtestproject.R;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;
import com.example.test.androidtestproject.databinding.ItemMainNewsBinding;

import java.util.ArrayList;
import java.util.List;

public class PreviewArticleAdapter extends RecyclerView.Adapter<PreviewArticleAdapter.PreviewArticleViewHolder> {

    private ItemClickListener itemClickListener;
    private final List<UIArticlePreview> itemContainer;

    public PreviewArticleAdapter() {
        this.itemContainer = new ArrayList<>();
    }

    @NonNull
    @Override
    public PreviewArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemMainNewsBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_main_news, parent, false);
        return new PreviewArticleViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviewArticleViewHolder holder, int position) {
        if (itemContainer.get(position) != null) {
            holder.bind(itemContainer.get(position), itemClickListener);
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void update(List<UIArticlePreview> itemContainer) {
        this.itemContainer.clear();
        this.itemContainer.addAll(itemContainer);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return itemContainer.size();
    }

    public interface ItemClickListener {
        void onItemClick(int itemId);
    }

    static class PreviewArticleViewHolder extends RecyclerView.ViewHolder {

        ItemMainNewsBinding binding;

        PreviewArticleViewHolder(@NonNull ItemMainNewsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(UIArticlePreview item, ItemClickListener onClickListener) {
            binding.setItem(item);
            binding.executePendingBindings();
            itemView.setOnClickListener(v -> onClickListener.onItemClick(item.getId()));
        }
    }
}
