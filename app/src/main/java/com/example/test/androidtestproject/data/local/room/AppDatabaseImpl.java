package com.example.test.androidtestproject.data.local.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.test.androidtestproject.data.model.data.DataArticle;

@Database(entities = DataArticle.class, version = 1, exportSchema = false)
public abstract class AppDatabaseImpl extends RoomDatabase implements AppDatabase{
    public static final String DATABASE_NAME = "article_database";

    public abstract ArticleDAOImpl breakingNewsDAO();
}
