package com.example.test.androidtestproject.data.local.shared_preferences;

import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;

abstract class SharedPreferencesIdentities {

    static final String DEFAULT_CATEGORY = Category.ENTERTAINMENT;
    static final String DEFAULT_COUNTRY = Country.US;
    static final String DEFAULT_LANGUAGE = Language.EN;

    static final String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_NAME";

    static final String PREFERENCES_CATEGORY = "PREFERENCES_CATEGORY";
    static final String PREFERENCES_COUNTRY = "PREFERENCES_COUNTRY";
    static final String PREFERENCES_LANGUAGE = "PREFERENCES_LANGUAGE";
}
