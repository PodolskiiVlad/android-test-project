package com.example.test.androidtestproject.data.local.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.test.androidtestproject.data.model.data.DataArticle;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public abstract class ArticleDAOImpl implements ArticleDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertArticles(List<DataArticle> articleList);

    @Query("SELECT * FROM table_articles")
    public abstract Flowable<List<DataArticle>> getArticleList();

    @Query("SELECT * FROM table_articles WHERE article_id=:id")
    public abstract Single<DataArticle> getArticle(int id);

    @Query("DELETE FROM table_articles")
    public abstract void clearBreakingNewsTable();
}
