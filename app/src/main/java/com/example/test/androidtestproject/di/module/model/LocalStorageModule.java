package com.example.test.androidtestproject.di.module.model;

import android.content.Context;

import androidx.room.Room;

import com.example.test.androidtestproject.data.local.room.AppDatabase;
import com.example.test.androidtestproject.data.local.room.AppDatabaseImpl;
import com.example.test.androidtestproject.data.local.room.ArticleDAO;
import com.example.test.androidtestproject.data.local.shared_preferences.SharedPreferencesImpl;
import com.example.test.androidtestproject.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalStorageModule {

    @Provides
    @AppScope
    SharedPreferencesImpl provideSharedPreferences(Context context) {
        return new SharedPreferencesImpl(context);
    }

    @Provides
    @AppScope
    AppDatabase provideAppDatabase(Context applicationContext) {
        return Room.databaseBuilder(applicationContext, AppDatabaseImpl.class, AppDatabaseImpl.DATABASE_NAME).build();
    }

    @Provides
    @AppScope
    ArticleDAO provideBreakingNewsDAO(AppDatabase appDatabase) {
        return appDatabase.breakingNewsDAO();
    }
}
