package com.example.test.androidtestproject.core;

import com.example.test.androidtestproject.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class NewsApplication extends DaggerApplication {

    private final AndroidInjector<NewsApplication> applicationInjector = DaggerAppComponent
            .builder()
            .application(this)
            .build();

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return applicationInjector;
    }
}
