package com.example.test.androidtestproject.data.network.retrofit.query_parameters;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.BUSINESS;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.ENTERTAINMENT;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.GENERAL;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.HEALTH;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.SCIENCE;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.SPORTS;
import static com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category.TECHNOLOGY;

@Retention(RetentionPolicy.RUNTIME)
@StringDef({
        BUSINESS, ENTERTAINMENT, GENERAL, HEALTH, SCIENCE, SPORTS, TECHNOLOGY
})
public @interface Category {
    String BUSINESS = "business";
    String ENTERTAINMENT = "entertainment";
    String GENERAL = "general";
    String HEALTH = "health";
    String SCIENCE = "science";
    String SPORTS = "sports";
    String TECHNOLOGY = "technology";
}
