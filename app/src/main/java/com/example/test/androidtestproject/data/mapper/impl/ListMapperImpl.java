package com.example.test.androidtestproject.data.mapper.impl;

import com.example.test.androidtestproject.data.mapper.ListMapper;
import com.example.test.androidtestproject.data.mapper.Mapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ListMapperImpl<I, O> implements ListMapper<I, O> {

    private Mapper<I, O> mapper;

    @Inject
    public ListMapperImpl(Mapper<I, O> mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<O> map(List<I> inputType) {
        List<O> outputList = new ArrayList<>();

        for (I remote : inputType) {
            outputList.add(mapper.map(remote));
        }

        return outputList;
    }
}
