package com.example.test.androidtestproject.view.binding;

import android.widget.Spinner;

import androidx.databinding.BindingAdapter;

public class SpinnerBinding {

    @BindingAdapter("selectedItem")
    public static void setSelectedItem(Spinner spinner, int position) {
        spinner.setSelection(position);
    }
}
