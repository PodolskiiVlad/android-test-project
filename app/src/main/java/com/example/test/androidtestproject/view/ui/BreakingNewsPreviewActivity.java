package com.example.test.androidtestproject.view.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.androidtestproject.R;
import com.example.test.androidtestproject.databinding.ActivityBreakingNewsBinding;
import com.example.test.androidtestproject.view.adapter.PreviewArticleAdapter;
import com.example.test.androidtestproject.viewmodel.BreakingNewsPreviewActivityViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

//TODO Add "Powered by News API string"
public class BreakingNewsPreviewActivity extends DaggerAppCompatActivity {

    @Inject
    BreakingNewsPreviewActivityViewModel viewModel;

    @Inject
    PreviewArticleAdapter adapter;

    @Inject
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityBreakingNewsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_breaking_news);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        adapter.setItemClickListener(itemId -> viewModel.onArticleClick(this, itemId));

        binding.mainNewsList.setLayoutManager(layoutManager);
        binding.mainNewsList.setAdapter(adapter);

        binding.newsRefreshLayout.setOnRefreshListener(viewModel::updateArticleList);

        viewModel.getArticleListLiveData().observe(this, list -> adapter.update(list));
        viewModel.getDataIsLoadingLiveData().observe(this, binding.newsRefreshLayout::setRefreshing);
        viewModel.getErrorLiveData().observe(this, this::buildAlertDialog);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.updateArticleList();
    }

    private void buildAlertDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(R.string.alert_title);
        alertDialog.setMessage(message);
        alertDialog.setNegativeButton(getString(R.string.error_negative_button_text), (dialog, which) -> dialog.dismiss());

        alertDialog.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuFilter) {
            viewModel.onFilterClick(this);
        }

        return super.onOptionsItemSelected(item);
    }
}
