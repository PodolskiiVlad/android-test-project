package com.example.test.androidtestproject.data.repository;

import android.util.Log;

import com.example.test.androidtestproject.data.local.room.ArticleDAO;
import com.example.test.androidtestproject.data.local.shared_preferences.SharedPreferencesImpl;
import com.example.test.androidtestproject.data.mapper.ListMapper;
import com.example.test.androidtestproject.data.mapper.Mapper;
import com.example.test.androidtestproject.data.model.data.DataArticle;
import com.example.test.androidtestproject.data.model.network.RemoteError;
import com.example.test.androidtestproject.data.model.network.response.RemoteArticle;
import com.example.test.androidtestproject.data.model.network.response.RemoteResponseHeadlineArticles;
import com.example.test.androidtestproject.data.model.ui.UIArticle;
import com.example.test.androidtestproject.data.model.ui.UIArticlePreview;
import com.example.test.androidtestproject.data.network.retrofit.NewsLoaderApi;
import com.example.test.androidtestproject.data.network.retrofit.RemoteErrorParser;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class ArticlePreviewRepositoryImpl implements ArticlePreviewRepository {

    private final ArticleDAO articleDAO;
    private final NewsLoaderApi newsLoaderApi;
    private final SharedPreferencesImpl sharedPreferences;

    private final ListMapper<RemoteArticle, DataArticle> dataListMapper;
    private final ListMapper<DataArticle, UIArticlePreview> previewMapper;

    private final Mapper<DataArticle, UIArticle> articleMapper;

    private final PublishSubject<RemoteError> errorPublisher;

    private final RemoteErrorParser errorParser;

    private Disposable requestDisposable;

    @Inject
    ArticlePreviewRepositoryImpl(
            RemoteErrorParser remoteErrorParser,
            SharedPreferencesImpl sharedPreferences,
            ArticleDAO articleDAO,
            NewsLoaderApi newsLoaderApi,
            ListMapper<DataArticle, UIArticlePreview> previewMapper,
            Mapper<DataArticle, UIArticle> articleMapper,
            ListMapper<RemoteArticle, DataArticle> dataListMapper
    ) {
        this.sharedPreferences = sharedPreferences;
        this.articleDAO = articleDAO;
        this.newsLoaderApi = newsLoaderApi;
        this.dataListMapper = dataListMapper;
        this.previewMapper = previewMapper;
        this.articleMapper = articleMapper;
        this.errorParser = remoteErrorParser;

        errorPublisher = PublishSubject.create();
    }

    @Override
    public void updateArticleList() {
        String country = getCountryPreference();
        String category = getCategoryPreference();
        String language = getLanguagePreference();

        requestDisposable = newsLoaderApi.getBreakingNewsArticles(category, language, country)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map(RemoteResponseHeadlineArticles::getArticleList)
                .map(dataListMapper::map)
                .doOnNext(dataArticles -> articleDAO.clearBreakingNewsTable())
                .doFinally(() -> requestDisposable.dispose())
                .subscribe(
                        articleDAO::insertArticles,
                        throwable -> {
                            Log.e(getClass().getSimpleName(), "updateArticleList: ", throwable);
                            errorPublisher.onNext(errorParser.parseError(throwable));
                        });
    }

    @Override
    public Flowable<List<UIArticlePreview>> getArticleFlowable() {
        return articleDAO
                .getArticleList()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map(previewMapper::map);
    }

    @Override
    public Single<UIArticle> getArticle(int articleId) {
        return articleDAO
                .getArticle(articleId)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map(articleMapper::map);
    }

    @Override
    public Observable<RemoteError> getErrorObservable() {
        return errorPublisher;
    }

    @Override
    public void savePreferences(@Category String category, @Language String language, @Country String country) {
        sharedPreferences.setCategoryPreference(category);
        sharedPreferences.setCountryPreference(country);
        sharedPreferences.setLanguagePreference(language);
    }

    @Override
    public @Country
    String getCountryPreference() {
        return sharedPreferences.getCountryPreference();
    }

    @Override
    public @Language
    String getLanguagePreference() {
        return sharedPreferences.getLanguagePreference();
    }

    @Override
    public @Category
    String getCategoryPreference() {
        return sharedPreferences.getCategoryPreference();
    }
}
