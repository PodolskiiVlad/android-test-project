package com.example.test.androidtestproject.data.local.shared_preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;

import javax.inject.Inject;

public class SharedPreferencesImpl {

    private final SharedPreferences sharedPreferences;

    @Inject
    public SharedPreferencesImpl(Context context) {
        this.sharedPreferences = context.getSharedPreferences(SharedPreferencesIdentities.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void setCategoryPreference(@Category String value){
        sharedPreferences.edit().putString(SharedPreferencesIdentities.PREFERENCES_CATEGORY, value).apply();
    }

    public @Category String getCategoryPreference(){
        return sharedPreferences.getString(SharedPreferencesIdentities.PREFERENCES_CATEGORY, SharedPreferencesIdentities.DEFAULT_CATEGORY);
    }

    public void setCountryPreference(@Country String value){
        sharedPreferences.edit().putString(SharedPreferencesIdentities.PREFERENCES_COUNTRY, value).apply();
    }

    public @Country String getCountryPreference(){
        return sharedPreferences.getString(SharedPreferencesIdentities.PREFERENCES_COUNTRY, SharedPreferencesIdentities.DEFAULT_COUNTRY);
    }

    public void setLanguagePreference(@Language String value) {
        sharedPreferences.edit().putString(SharedPreferencesIdentities.PREFERENCES_LANGUAGE, value).apply();
    }

    public @Language String getLanguagePreference(){
        return sharedPreferences.getString(SharedPreferencesIdentities.PREFERENCES_LANGUAGE, SharedPreferencesIdentities.DEFAULT_LANGUAGE);
    }
}
