package com.example.test.androidtestproject.di;

import com.example.test.androidtestproject.di.module.view.NewsFilterDialogModule;
import com.example.test.androidtestproject.di.scope.DialogScope;
import com.example.test.androidtestproject.view.ui.NewsFilterDialog;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class DialogBuildersModule {

    @DialogScope
    @ContributesAndroidInjector(modules = {NewsFilterDialogModule.class})
    abstract NewsFilterDialog bindNewsFilterDialog();

}
