package com.example.test.androidtestproject.view.binding;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.example.test.androidtestproject.R;
import com.squareup.picasso.Picasso;

public class ImageViewBinding {

    @BindingAdapter("imageSource")
    public static void setImageSource(ImageView imageView, String url) {
        if (url == null || url.isEmpty()) {
            imageView.setImageResource(R.drawable.ic_no_image);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            return;
        }

        Picasso.get().load(url).error(R.drawable.ic_no_image).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }
}
