package com.example.test.androidtestproject.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.example.test.androidtestproject.R;
import com.example.test.androidtestproject.databinding.DialogNewsFilterBinding;
import com.example.test.androidtestproject.viewmodel.NewsFilterDialogViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerDialogFragment;

public class NewsFilterDialog extends DaggerDialogFragment {

    @Inject
    NewsFilterDialogViewModel viewModel;

    private DialogNewsFilterBinding binding;

    public static DialogFragment getInstance() {
        return new NewsFilterDialog();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_news_filter, container, false);
        binding.setViewModel(viewModel);

        viewModel.getDismissLiveData().observe(getViewLifecycleOwner(), aVoid -> dismiss());
        setSpinnerListeners();

        return binding.getRoot();
    }

    private void setSpinnerListeners() {
        binding.filterCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.setCountryPreference((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.filterLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.setLanguagePreference((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.filterCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.setCategoryPreference((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
