package com.example.test.androidtestproject.viewmodel;

import android.content.Context;

import androidx.annotation.ArrayRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test.androidtestproject.R;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Category;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Country;
import com.example.test.androidtestproject.data.network.retrofit.query_parameters.Language;
import com.example.test.androidtestproject.data.repository.ArticlePreviewRepository;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

public class NewsFilterDialogViewModel extends ViewModel {

    private final MutableLiveData<Void> dismissAction;

    private final ArticlePreviewRepository repository;

    private final Map<String, String> countryPreferenceMap;
    private final Map<String, String> languagePreferenceMap;
    private final Map<String, String> categoryPreferenceMap;

    private @Country
    String selectedCountry;
    private @Language
    String selectedLanguage;
    private @Category
    String selectedCategory;

    @Inject
    public NewsFilterDialogViewModel(ArticlePreviewRepository repository, Context context) {
        this.repository = repository;

        dismissAction = new MutableLiveData<>();

        countryPreferenceMap = getMapFromResourceArray(R.array.preferenceCountry, context);
        languagePreferenceMap = getMapFromResourceArray(R.array.preferenceLanguage, context);
        categoryPreferenceMap = getMapFromResourceArray(R.array.preferenceCategory, context);

        selectedCountry = repository.getCountryPreference();
        selectedLanguage = repository.getLanguagePreference();
        selectedCategory = repository.getCategoryPreference();
    }

    public int getCountryPreferencePosition() {
        return getValueIndex(repository.getCountryPreference(), countryPreferenceMap);
    }

    public int getLanguagePreferencePosition() {
        return getValueIndex(repository.getLanguagePreference(), languagePreferenceMap);
    }

    public int getCategoryPreferencePosition() {
        return getValueIndex(repository.getCategoryPreference(), categoryPreferenceMap);
    }

    public String[] getAvailableCountries() {
        return countryPreferenceMap.keySet().toArray(new String[0]);
    }

    public String[] getAvailableLanguages() {
        return languagePreferenceMap.keySet().toArray(new String[0]);
    }

    public String[] getAvailableCategories() {
        return categoryPreferenceMap.keySet().toArray(new String[0]);
    }

    public void setLanguagePreference(String preference) {
        selectedLanguage = languagePreferenceMap.get(preference);
    }

    public void setCountryPreference(String preference) {
        selectedCountry = countryPreferenceMap.get(preference);
    }

    public void setCategoryPreference(String preference) {
        selectedCategory = categoryPreferenceMap.get(preference);
    }

    public void onSaveClick() {
        repository.savePreferences(selectedCategory, selectedLanguage, selectedCountry);
        dismissAction.postValue(null);
    }

    public void onCancelClick() {
        dismissAction.postValue(null);
    }

    public LiveData<Void> getDismissLiveData() {
        return dismissAction;
    }

    private int getValueIndex(String key, Map<String, String> map) {
        Iterator valueIterator = map.values().iterator();

        for (int i = 0; valueIterator.hasNext(); ++i) {
            String value = (String) valueIterator.next();
            if (key.equalsIgnoreCase(value)) return i;
        }

        return 0;
    }

    private Map<String, String> getMapFromResourceArray(@ArrayRes int arrayResource, Context context) {
        Map<String, String> resultMap = new TreeMap<>();

        String[] resourceArray = context.getResources().getStringArray(arrayResource);

        for (String value : resourceArray) {
            String[] splittedValue = value.split("\\|", 2);
            resultMap.put(splittedValue[1], splittedValue[0]);
        }

        return resultMap;
    }
}
