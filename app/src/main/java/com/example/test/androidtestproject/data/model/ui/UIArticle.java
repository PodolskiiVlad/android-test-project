package com.example.test.androidtestproject.data.model.ui;

import androidx.annotation.NonNull;

public class UIArticle {

    @NonNull
    private String imageUrl;

    @NonNull
    private String title;

    @NonNull
    private String description;

    @NonNull
    private String publishedAt;

    @NonNull
    private String author;

    @NonNull
    private String sourceUrl;

    public UIArticle(@NonNull String imageUrl,
                     @NonNull String title,
                     @NonNull String description,
                     @NonNull String publishedAt,
                     @NonNull String author,
                     @NonNull String sourceUrl) {
        this.imageUrl = imageUrl;
        this.title = title;
        this.description = description;
        this.publishedAt = publishedAt;
        this.author = author;
        this.sourceUrl = sourceUrl;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    @NonNull
    public String getPublishedAt() {
        return publishedAt;
    }

    @NonNull
    public String getAuthor() {
        return author;
    }

    @NonNull
    public String getSourceUrl() {
        return sourceUrl;
    }
}
