package com.example.test.androidtestproject.data.model.network.response;

import com.google.gson.annotations.SerializedName;

public class RemoteSource {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
