package com.example.test.androidtestproject.di;

import com.example.test.androidtestproject.di.module.view.ArticleFragmentModule;
import com.example.test.androidtestproject.di.module.view.BreakingNewsPreviewActivityModule;
import com.example.test.androidtestproject.di.scope.ActivityScope;
import com.example.test.androidtestproject.view.ui.ArticleActivity;
import com.example.test.androidtestproject.view.ui.BreakingNewsPreviewActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBuildersModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {BreakingNewsPreviewActivityModule.class})
    abstract BreakingNewsPreviewActivity bindBreakingNewsActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = ArticleFragmentModule.class)
    abstract ArticleActivity bindArticleFragment();
}
