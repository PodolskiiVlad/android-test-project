package com.example.test.androidtestproject.data.local.room;

import com.example.test.androidtestproject.data.model.data.DataArticle;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface ArticleDAO {

    void insertArticles(List<DataArticle> articleList);

    Flowable<List<DataArticle>> getArticleList();

    Single<DataArticle> getArticle(int id);

    void clearBreakingNewsTable();
}
