package com.example.test.androidtestproject.data.mapper.impl;

import com.example.test.androidtestproject.data.mapper.Mapper;
import com.example.test.androidtestproject.data.model.data.DataArticle;
import com.example.test.androidtestproject.data.model.network.response.RemoteArticle;
import com.example.test.androidtestproject.data.model.network.response.RemoteSource;

import javax.inject.Inject;

public class RemoteArticleToDataArticleMapper implements Mapper<RemoteArticle, DataArticle> {

    @Inject
    RemoteArticleToDataArticleMapper() {
    }

    @Override
    public DataArticle map(RemoteArticle inputType) {
        DataArticle dataArticle = new DataArticle();
        RemoteSource remoteSource = inputType.getRemoteSource();

        if (remoteSource != null) {
            dataArticle.setSourceName(inputType.getRemoteSource().getName());
            dataArticle.setSourceId(inputType.getRemoteSource().getId());
        }

        dataArticle.setAuthor(inputType.getAuthor());
        dataArticle.setTitle(inputType.getTitle());
        dataArticle.setDescription(inputType.getDescription());
        dataArticle.setUrlToImage(inputType.getUrlToImage());
        dataArticle.setSourceUrl(inputType.getUrl());
        dataArticle.setPublishedAt(inputType.getPublishedAt());
        dataArticle.setContent(inputType.getContent());

        return dataArticle;
    }
}
