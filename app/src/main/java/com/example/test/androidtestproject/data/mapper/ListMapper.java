package com.example.test.androidtestproject.data.mapper;

import java.util.List;

public interface ListMapper<I, O> extends Mapper<List<I>, List<O>> {
}
