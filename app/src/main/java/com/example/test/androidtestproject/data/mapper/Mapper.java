package com.example.test.androidtestproject.data.mapper;

public interface Mapper<I, O> {

    String DEFAULT_STRING = "";

    O map(I inputType);
}
