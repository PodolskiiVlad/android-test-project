<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.example.test.androidtestproject.viewmodel.ArticleActivityViewModel" />
    </data>

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@android:color/white">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@android:color/white"
            android:orientation="vertical">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/titleContainer"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:background="@android:color/white"
                android:elevation="2dp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintVertical_chainStyle="packed">

                <ImageView
                    android:id="@+id/articleTitleImage"
                    imageSource="@{viewModel.imageUrl}"
                    android:layout_width="0dp"
                    android:layout_height="0dp"
                    android:background="@android:color/black"
                    android:elevation="1dp"
                    android:src="@drawable/ic_no_image"
                    app:layout_constraintDimensionRatio="H,16:9"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    app:layout_constraintVertical_chainStyle="packed" />

                <TextView
                    android:id="@+id/articleTitle"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/article_text_margin_horizontal"
                    android:layout_marginTop="8dp"
                    android:layout_marginEnd="@dimen/article_text_margin_horizontal"
                    android:layout_marginBottom="8dp"
                    android:text="@{viewModel.title}"
                    android:textColor="@android:color/black"
                    android:textSize="24sp"
                    android:textStyle="bold"
                    app:layout_constraintBottom_toTopOf="@id/articleAuthor"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/articleTitleImage" />

                <TextView
                    android:id="@+id/articleAuthor"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/article_text_margin_horizontal"
                    android:layout_marginEnd="@dimen/article_text_margin_horizontal"
                    android:text='@{@string/article_author + viewModel.author}'
                    android:textColor="@android:color/black"
                    android:visibility='@{viewModel.author.equals("") ? View.GONE : View.VISIBLE}'
                    app:layout_constraintBottom_toTopOf="@id/articlePublishedAt"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/articleTitle" />

                <TextView
                    android:id="@+id/articlePublishedAt"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/article_text_margin_horizontal"
                    android:layout_marginEnd="@dimen/article_text_margin_horizontal"
                    android:layout_marginBottom="8dp"
                    android:text="@{viewModel.publishedAt}"
                    android:textSize="12sp"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/articleAuthor" />

            </androidx.constraintlayout.widget.ConstraintLayout>

            <TextView
                android:id="@+id/articleDescriptionStatic"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/article_text_margin_horizontal"
                android:layout_marginTop="12dp"
                android:layout_marginEnd="@dimen/article_text_margin_horizontal"
                android:text="@string/article_description"
                android:textColor="@android:color/black"
                android:textSize="20sp"
                android:textStyle="bold"
                app:layout_constraintBottom_toTopOf="@id/articleDescription"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/titleContainer" />

            <TextView
                android:id="@+id/articleDescription"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/article_text_margin_horizontal"
                android:layout_marginTop="8dp"
                android:layout_marginEnd="@dimen/article_text_margin_horizontal"
                android:text='@{viewModel.description.equals("") ? @string/article_default_description : viewModel.description}'
                android:textColor="@android:color/black"
                android:textSize="18sp"
                app:layout_constraintBottom_toTopOf="@+id/articleButton"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/articleDescriptionStatic" />

            <Button
                android:id="@+id/articleButton"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/article_button_margin_horizontal"
                android:layout_marginTop="32dp"
                android:layout_marginEnd="@dimen/article_button_margin_horizontal"
                android:layout_marginBottom="16dp"
                android:background="@drawable/bg_article_button"
                android:elevation="8dp"
                android:onClick="@{v -> viewModel.jumpToSource(v.getContext())}"
                android:text="@string/article_button_title"
                android:textAllCaps="false"
                android:textColor="@android:color/white"
                android:textSize="18sp"
                android:textStyle="bold"
                android:visibility="@{viewModel.sourceUrl.empty ? View.INVISIBLE : View.VISIBLE}"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintDimensionRatio="12:2"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/articleDescription" />
        </androidx.constraintlayout.widget.ConstraintLayout>
    </ScrollView>
</layout>